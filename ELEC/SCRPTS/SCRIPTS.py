import pcbnew
def HIDEREFF():#Hide all references
    pcb = pcbnew.GetBoard()

    if hasattr(pcb, 'GetModules'):
            modules = pcb.GetModules()
        else:
            modules = pcb.GetFootprints()

        for module in modules:
            module.Reference().SetVisible(False)
